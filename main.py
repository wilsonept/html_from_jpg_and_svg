import os
from jinja2 import Template

"""
Скрипт формирования html страницы на основе jpg подложки и svg контента.
"""

IMG_DIR = "<путь-до-папки-с-изображениями>"
SVG_DIR = "<путь-до-папки-с-SVG>"
OUT_NAME = "<полное-навание-будущего-html-файла>"
PAGE_WIDTH = 680

# Получаем списки файлов подложек и  файлов контента.
imgs = os.listdir(IMG_DIR)
svgs = os.listdir(SVG_DIR)

# Определяем в каком из списков меньше объектов.
diff = len(imgs) - len(svgs)
if diff < 0:
    for _ in range(diff):
        # Добавляем недостающие элементы для zip.
        imgs.append("")
elif  diff > 0:
    for _ in range(diff):
        # Добавляем недостающие элементы для zip.
        svgs.append("")

# Загружаем контент шаблона и создаем шаблон.
with open(file="base.html") as f:
    content = f.read()

template = Template(source=content)

# Формируем список страниц.
pages = []
for img, svg in zip(imgs, svgs):
    pages.append((img, svg))

# Рендерим шаблон с переданными страницами.
result = template.render(pages=pages, page_width=PAGE_WIDTH)

# Сохраняем результат в файл.
with open(file=OUT_NAME, mode="w", encoding="utf-8") as f:
    f.write(result)